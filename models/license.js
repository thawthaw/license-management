﻿var fs = require('fs');
var config = require('../config.json');

// check file exists
var dbfile = config.database.path + config.database.name;
var exists = fs.existsSync(dbfile);

if (!exists) {
    //console.log("Creating DB file.");
    fs.openSync(dbfile, "w");
}

// initialize database connection
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(dbfile);

// Database initialization
db.serialize(function () {
    db.run("CREATE TABLE IF NOT EXISTS License ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            "key TEXT NOT NULL UNIQUE, " +
            "email TEXT NOT NULL, " +
            "origin TEXT NULL, " +
            "digest TEXT NULL, " +
            "issued DATE NOT NULL, " +
            "expire DATE NOT NULL)");
    
    db.run("CREATE UNIQUE INDEX IF NOT EXISTS LicenseKeyIndex ON License (key)");
     
});

module.exports = db;