﻿var express = require('express');
var router = express.Router();
var licenseModel = require('../models/license');

function loggedIn(req, res, next) {
    if (req.session.login != undefined) {
        next();
    } else {
        res.json({ 'status': 'error', 'message': 'unauthorize access' });
    }
}

function generateLicenseKey() {
    var parts = [];
    
    for (var i = 0; i < 4; i++) {
        parts.push(Math.random().toString(36).substr(2, 5));
    }
    
    return parts.join('-').toUpperCase();
}

/* GET licenses listing. */
router.get('/', loggedIn, function (req, res) {
    var offset = (req.query.offset!=undefined)? req.query.offset:0;
    var limit= (req.query.limit!=undefined)? req.query.limit: 20;
    var keyword = (req.query.keyword!='')? "%"+req.query.keyword+"%":"";
    var sortedby = (req.query.sortedby!='')? req.query.sortedby:"id";
    var dir = (req.query.dir != '')? req.query.dir:"DESC";
    
    var params = {};

    var total = "SELECT COUNT(*) as total FROM License";
    var query = "SELECT * FROM License ";
    if (keyword != "") {
        query += " WHERE email LIKE $keyword OR key LIKE $keyword OR origin LIKE $keyword";
        total += " WHERE email LIKE $keyword OR key LIKE $keyword OR origin LIKE $keyword";
        params.$keyword=keyword;
    }
    
    // get total first

    licenseModel.get(total, params, function (err, count) {
        if (err != null) {
            //console.log(err);
            res.json({ 'status': 'error' });
        } else {
            var cols = ["id", "key", "email", "origin", "digest", "issued", "expire"];
            if (cols.indexOf(sortedby) > -1) {
                query += " ORDER BY " + (cols.indexOf(sortedby) + 1);
            } else {
                query += " ORDER BY id";
            }
            
            if (dir == "ASC") {
                query += " ASC";
            } else {
                query += " DESC";
            }
            query += " LIMIT $offset,$limit";
            
            params.$offset = offset;
            params.$limit = limit;
            
           // console.log(params);

            // retrieve related records
            licenseModel.all(query, params, function (err, rows) {
                if (err != null) {
                    console.log(err);
                    res.json({ 'status': 'error' });
                } else {
                    res.json({ 'status': 'success', 'data': rows, 'offset': offset, 'limit': limit, 'keyword': req.query.keyword, 'sortedby': sortedby, 'dir': dir, 'total': count.total });
                   
                }
            })
        }
       
    })    
    
    
     
});

/* POST licenses */
router.post('/', loggedIn, function (req, res) {
    
    if (req.body.email != undefined && req.body.expired != undefined) {
        var email = req.body.email;
        var expired = req.body.expired;
        var origin = req.body.origin;
        var multiplier = req.body.multiplier;
        
        // prepare insert statement   
        var stmt = licenseModel.prepare("INSERT INTO License(key,email,origin,expire,issued) VALUES (?,?,?,?,datetime('now'))");
        var i=1;
        while(i<= multiplier){
            var key = "";
            
            do {
                
                // generate key
                key = generateLicenseKey();
                
                // check key is already inside db
                // generate key until there is no duplicate inside db
                licenseModel.all("SELECT * FROM License WHERE key=?", key, function (err, row) {
                    if(row.length>0) key="";
                })
            } while(key == "");
            
            stmt.run([key, email, origin, expired]);
            i++;
        }
        
        res.json({ 'status': 'success' });
    } else { 
        res.json({ 'status': 'error', 'message': 'Invalid fields' });
    }

 
})

/* DELETE licenses */
router.delete('/', loggedIn, function (req, res) {
    //delete licenses
    var response = {};
    var ids = req.body.ids;
    if (ids != "") {
        var id_array = JSON.parse("[" + ids + "]");
        var param_array = [];
        var query = "DELETE FROM License WHERE id IN (";
        for (i = 0; i <= id_array.length - 1; i++) {
            param_array.push('?');
        }
        query += param_array.join(',') + ")";

        licenseModel.run(query, id_array, function (err){
            if (err != null) {
                res.json({ 'status': 'error', 'message': 'internal server error' });
            } else {
                res.json({ 'status': 'success' });
            }
        })

    } else {
        res.json({ 'status': 'error', 'message': 'Nothing to delete' });
    }

    
});


module.exports = router;