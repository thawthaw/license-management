﻿var express = require('express');
var config = require('../config.json');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    if (req.session.login != undefined) {
        res.sendfile('index.html')
    } else {
        res.sendfile('login.html')
    }
   
});

/** Check login ***/
router.post('/login', function (req, res) {
    if (req.session.login != undefined) {
        res.json({ 'status': 'login' });
    } else {
        res.json({ 'status': 'invalid' });
    }
});
router.post('/logout', function (req, res) {
    if (req.session.login != undefined) {
        req.session.destroy();
        res.json({ 'status': 'logout' });
    } else {
        res.json({ 'status': 'invalid' });
    }
})

/** Authorize user ***/
router.post('/authorize', function (req, res){
    if (req.body.username != undefined && req.body.password != undefined) {
        if (req.body.username == config.login.username && req.body.password == config.login.password) {
            req.session.login = 'admin';
            res.json({ 'status': 'success' });
        } else {
            res.json({ 'status': 'Invalid username or password' });
        }
    } else {
        res.json({ 'status': 'Invalid username or password' });
    }
})

module.exports = router;