﻿'use strict'; 
/*** login controller ***/
app.controller('LoginCtrl', ["$scope", '$rootScope','APIService','$location',
    function ($scope, $rootScope, APIService, $location) {
        $scope.alerts = [];
        $scope.login = function () {
            // Ask to the server, do your job and THEN set the user
            APIService.authorize({username:$scope.username,password:$scope.password}, function (error, data) {
                if (!error) {
                    console.log(data)
                    if (data.status == "success") {
                        
                        $rootScope.loggedInUser = 'admin';
                        $location.path("/home");
                    } else {
                        $scope.loginfailed = true;
                        $scope.alerts=[{ 'type': 'danger', 'msg': 'Invalid username or password' }];
                    }
                }
            }); 
            
        }
    }]);

/*** license controller ****/
app.controller('LicenseCtrl', ["$scope", '$rootScope','$cookieStore', 'APIService','$location','$uibModal','$confirm',
    function ($scope, $rootScope, $cookieStore, APIService,$location,$uibModal,$confirm) {
        $scope.query = {};
        $scope.alerts = [];
        $scope.selected = [];
        $scope.keyword='';
        
        // retrieve query from cookie
        if ($cookieStore.get('query') != null) {
           
            $scope.query = $cookieStore.get('query');
            console.log($scope.query);
        } else {
            $scope.query = { 'offset': 0, 'limit': 20, 'dir':'DESC','sortedby':'issued','keyword':'','page':1,'total':0 };
        }
        
        function getData(){
            
            APIService.getlicenses($scope.query, function (err, result) {
                if (!err) {
                    console.log(result);
                    if (result.status == 'success') {
                        
                        $scope.query.dir = result.dir;
                        $scope.query.keyword = result.keyword;
                        $scope.query.limit = result.limit;
                        $scope.query.offset = result.offset;
                        $scope.query.sortedby = result.sortedby;
                        $scope.query.page = $scope.query.page;
                        $scope.query.total = result.total;
                        // store the query inside cookie
                        $cookieStore.put('query', $scope.query);

                        $scope.licenses = result.data;
                        $scope.keyword= result.keyword;
                    } else {
                        if (result.message == "unauthorize access") {
                             $location.path("/login");
                        }
                    }
                } else {
                     $scope.alerts = [{ 'type': 'danger', 'msg': 'Internal server error.' }];
                }
            });

        }
        
        $scope.searchKeyword = function (){
            console.log($scope.query.keyword);
            $scope.query.page = 1;
            $scope.query.offset = 0;
            getData();
        }
        
        $scope.setPage = function (pageNo) {
            $scope.query.page = pageNo;
        };
        // retrieve records
        $scope.pageChanged = function () {
            console.log('Page changed to: ' + $scope.query.page);
            $scope.query.offset = (($scope.query.page - 1) * $scope.query.limit);
            getData();
        };
        $scope.sortedbyChanged = function (sortedby) {
            if($scope.query.sortedby==sortedby){
                if($scope.query.dir=='ASC') $scope.query.dir='DESC';
                else $scope.query.dir='ASC';
            }else{
                 $scope.query.sortedby=sortedby;
                 $scope.query.dir='ASC';
            }
            $scope.query.page = 1;
            $scope.query.offset = 0;
            getData();
        }    
        $scope.dirChanged = function (dir) {
            $scope.query.dir=dir;
            $scope.query.page = 1;
            $scope.query.offset = 0;
            getData();
        }
        
        $scope.addLisenses = function (){   
            var modalInstance = $uibModal.open({
                templateUrl: './javascripts/views/addnew.html',
                controller: 'addCtrl'
                
            });
            
            modalInstance.result.then(function (selectedItem) {
                getData();
            }, function () {
                //console.log('Modal dismissed at: ' + new Date());
            });
        };
        $scope.toggleSelection = function toggleSelection(id) {
            var idx = $scope.selected.indexOf(id);
            
            // is currently selected
            if (idx > -1) {
                $scope.selected.splice(idx, 1);
            }

            // is newly selected
            else {
                $scope.selected.push(id);
            }
        }
        $scope.deleteLicenses = function (){
            $confirm({ text: 'Are you sure you want to delete?' })
            .then(function (){
                 
                console.log($scope.selected);
                var removeQuery = { 'ids': $scope.selected.join(',') };
                APIService.deleteLicense(removeQuery, function (err, result) {
                    if (!err) {
                        console.log(result);
                        if (result.status == 'success') {                            
                            getData();
                        } else {
                            if (result.message == "unauthorize access") {
                                $location.path("/login");
                            }
                        }
                    } else {
                        $scope.alerts = [{ 'type': 'danger', 'msg': 'Internal server error.' }];
                    }
                })
            })
        }
        
        $scope.logout = function (){
            console.log('logout');
            APIService.loggedOut(function (error, data) {
                if (!error) {
                    console.log(data)
                    if (data.status == "logout") {
                        
                        $rootScope.loggedInUser = '';
                        $location.path("/login");
                    }
                }
            }); 

        }
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };
        // call first set of records
        getData();
    }]);


/***** Adding new license controller ****/
app.controller('addCtrl', ["$scope", '$rootScope','$cookieStore', 'APIService','$location','$uibModalInstance',
    function ($scope, $rootScope, $cookieStore, APIService, $location, $uibModalInstance) {
        $scope.alerts = [];
        $scope.closed = false;
        $scope.calendar= false;
        $scope.format = 'yyyy-MM-dd';
        $scope.openCalendar = function ($event) {
            $scope.calendar= true;
        };
        $scope.save = function (){
            var expireDate= new Date($scope.expired).toISOString().slice(0, 19).replace('T', ' ');
            var license = { 'email': $scope.email, 'origin': $scope.origin, 'digest': $scope.digest, 'expired': expireDate , 'multiplier': $scope.multiplier };
             
            APIService.saveLicense(license, function (err, result) {
                if (!err) {
                    console.log(result);
                    $scope.closed = true;
                    if (result.status == 'success') {
                        $uibModalInstance.close();
                    } else {
                        if (result.message == "unauthorize access") {
                            $uibModalInstance.dismiss('cancel');
                            $location.path("/login");
                        }
                    }
                } else {
                    $scope.alerts.push({ 'type': 'danger', 'msg': 'Internal server error.' });
                }
            });

        }
        $scope.cancel = function () {
            $uibModalInstance.close();
        };
    }])
