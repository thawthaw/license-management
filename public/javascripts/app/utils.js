﻿
/***** DIRECTIVES ********/
app.directive('charLimit', function () {
    return {
        restrict: 'A',
        link: function ($scope, $element, $attributes) {
            var limit = $attributes.charLimit;
            
            $element.bind('keyup', function (event) {
                var element = $element.parent().parent();
                
                element.toggleClass('warning', limit - $element.val().length <= 10);
                element.toggleClass('error', $element.val().length > limit);
            });
            
            $element.bind('keypress', function (event) {
                // Once the limit has been met or exceeded, prevent all keypresses from working
                if ($element.val().length >= limit) {
                    // Except backspace
                    if (event.keyCode != 8) {
                        event.preventDefault();
                    }
                }
            });
        }
    };
});

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9+-]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                
                return transformedInput;
            });
        }
    };
});
app.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if(attrs.ngClick || attrs.href === '' || attrs.href === '#'){
                elem.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
});
app.directive('eatClick', function () {
    return function (scope, element, attrs) {
        $(element).click(function (event) {
            event.preventDefault();
        });
    }
})

app.filter('truncate', function () {
    return function (text, length, end) {
        if (isNaN(length))
            length = 10;
        
        if (end === undefined)
            end = "...";
        
        if (text.length <= length || text.length - end.length <= length) {
            return text;
        }
        else {
            return String(text).substring(0, length - end.length) + end;
        }

    };
});

app.filter('position', function () {
    return function (input) {
        if (parseInt(input) < 10) {
            return input = "0" + input;
        } else {
            return input;
        }
    };
});

app.filter('ordinal', function () {
    return function (input) {
        var num = parseInt(input, 10);
        
        return Math.floor(num / 10) === 1
      ? 'th'
      : (num % 10 === 1
        ? 'st'
        : (num % 10 === 2
          ? 'nd'
          : (num % 10 === 3
            ? 'rd'
            : 'th')));
    };
});
app.filter('highlight', function ($sce) {
    return function (text, phrase) {
        if (phrase) text = text.replace(new RegExp('(' + phrase + ')', 'gi'),
        '<span class="highlighted">$1</span>')
        
        return $sce.trustAsHtml(text)
    }
});

app.filter('dateOnly',function($sce){
     return function(input){
         return $sce.trustAsHtml(input.split(' ')[0]);
     }
});
 