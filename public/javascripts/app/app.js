﻿'use strict';

/**
 * @ngdoc overview
 * @name LicenseApp
 * @description
 * # LicenseApp
 *
 * Main module of the application.
 */
var app = angular
  .module('licenseApp', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngMessages',
    'ui.bootstrap',
    'angular-confirm'

]);
app.config(function ($routeProvider, $locationProvider) {
       // $locationProvider.html5Mode(true);
        
        $routeProvider
        .when('/', {
            templateUrl: './javascripts/views/login.html',
            controller: 'LoginCtrl',
            
            resolve: function () {
                if ($rootScope.loggedInUser != null) {
                    // no logged user, redirect to /login
                    //console.log('go to home');
                    $location.path("/home");
                    
                }
            }
        }) 
        .when('/home',{
            templateUrl: './javascripts/views/home.html',
            controller: 'LicenseCtrl'
        })
        .when('/login', {
            templateUrl: './javascripts/views/login.html',
            controller: 'LoginCtrl'
        });
    })
    .run(function ($rootScope,$location,APIService,$http) {
    $rootScope.isLocal = false;
    $rootScope.baseurl = './javascripts/';    
    $http.get($rootScope.baseurl+'config.json').then((res)=>{
        $rootScope.apiurl = res.data.api_url;
        $rootScope.$on('$locationChangeStart', function (event) {
        //console.log('go to login');
        APIService.isLoggedIn( function (error, data) {
            if (!error) {
                //console.log(data)
                if (data.status == "invalid") {
                    
                    $rootScope.loggedInUser = '';
                    $location.path("/login");
                }else{
                    $location.path("/home");
                    }  
                }
            }); 
        });
    });

});
